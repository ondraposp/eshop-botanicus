<?php

function woocommerce_ppl_parcelshop_kuryr_shipping_init(){


if ( !class_exists( 'WC_Shipping_Method' ) ) 
      return;



 
		if ( ! class_exists( 'WC_Ppl_Parcelshop_Kuryr_Shipping_Method' ) ) {
			class WC_Ppl_Parcelshop_Kuryr_Shipping_Method extends WC_Shipping_Method {
				
				/**
  				 * Plugin slug
  				 *
  				 */        
  				private $plugin_slug = 'woo-ppl-parcelshop';

				/**
				 * Constructor for your shipping class
				 *
				 * @access public
				 * @return void
				 */
				public function __construct( $instance_id = 0 ) {
					$this->id                 = 'ppl-parcelshop-kuryr'; 
					$this->instance_id        = absint( $instance_id );
                    $this->method_title       = __( 'PPL Kurýr', $this->plugin_slug );
                    $this->method_description = __( 'PPL Kurýr pro eshop ', $this->plugin_slug );

                    $this->supports           = array(
                                                'shipping-zones',
                                                'instance-settings',
                                                'instance-settings-modal',
                    );

                    $this->init_form_fields();

                    $this->enabled            = "yes";
                    $this->title              = $this->get_option( 'title' );
                    $this->description        = $this->get_option( 'description' );
                    $this->availability       = $this->get_option( 'availability' );
                    $this->cost               = $this->get_option( 'cost' );
                    
                    add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
        
          
				}
 
                function init_form_fields() {
		
					$this->instance_form_fields = array(
						'enabled' => array(
							'title'       => __( 'Povolit DPD Kurýr', 'woo-dpd-parcelshop' ),
							'type' 	      => 'checkbox',
							'label'       => __( 'Povolit tento způsob', 'woo-dpd-parcelshop' ),
							'default'     => 'no',
							),
						'title' => array(
							'title'       => __( 'Název metody', 'woo-dpd-parcelshop' ),
							'type'        => 'text',
							'description' => __( '', 'woo-dpd-parcelshop' ),
							'default'     => __( 'PPL Kurýr', 'woo-dpd-parcelshop' ),
							),
						'description' => array(
							'title'       => __( 'Vyzvednutí na odběrném místě Parcelshopu', 'woo-dpd-parcelshop' ),
							'type'        => 'text',
							'description' => __( 'Text, který zákazník uvidí', 'woo-dpd-parcelshop' ),
							'default'     => 'Vyzvednutí na odběrném místě Parcelshopu',
						),
                        'cost' => array(
                            'title'       => __( 'Cena za dopravu', 'woo-ppl-parcelshop' ),
                            'type'        => 'price',
                            'description' => __( 'Cena za dopravu', 'woo-ppl-parcelshop' ),
                            'default'     => '90',
                        ),
                        'dobirka' => array(
                            'title'       => __( 'Příplatek za dobírku', 'woo-ppl-parcelshop' ),
                            'type'        => 'price',
                            'description' => __( 'Příplatek za dobírku', 'woo-ppl-parcelshop' ),
                            'default'     => '20',
                        )
					);

				}
        
        
 
				/**
				 * calculate_shipping function.
				 *
				 */
				public function calculate_shipping( $package = array() ) {
					
                    //Define rates array
                    $rates = array();
                    //Create rates important values
                    $rates['id']       = $this->id. '>' .$this->instance_id;
                    $rates['label']    = $this->title;
                    $rates['calc_tax'] = 'per_order';
                    $rates['cost']     = $this->cost;
          
                    $this->add_rate( $rates );
                     
			    }
        
         
      
                /**
                 * Is available shipping
                 *
                 */                    
                public function is_available( $package = array() ){
    
                    if ( 'no' == $this->enabled ) {
                        return false;
                    }             

                    return apply_filters( 'woocommerce_shipping_' . $this->id . '_is_available', true, $package );
      
                } 
        
			}//End class 
      
		}
                                            
}
add_action('plugins_loaded', 'woocommerce_ppl_parcelshop_kuryr_shipping_init');
 
	function add_woo_ppl_parcelshop_kuryr_shipping_method( $methods ) {
		$methods['ppl-parcelshop-kuryr'] = 'WC_Ppl_Parcelshop_Kuryr_Shipping_Method';
		return $methods;
	}
  add_filter( 'woocommerce_shipping_methods', 'add_woo_ppl_parcelshop_kuryr_shipping_method' );



?>