<?php

function woocommerce_pplparcelshop_shipping_init(){


if ( !class_exists( 'WC_Shipping_Method' ) ) 
      return;



 
		if ( ! class_exists( 'WC_Pplparcelshop_Shipping_Method' ) ) {
			class WC_Pplparcelshop_Shipping_Method extends WC_Shipping_Method {
				/**
				 * Constructor for your shipping class
				 *
				 * @access public
				 * @return void
				 */
				public function __construct( $instance_id = 0 ) {

					$this->id                 = 'pplparcelshop'; 
					$this->instance_id        = absint( $instance_id );
					
					$this->method_title       = 'PPL Parcelshop';
					$this->method_description = 'Vyzvednutí na pobočce PPL Parcelshop';
					

					$this->supports           = array(
													'shipping-zones',
													'instance-settings',
													'instance-settings-modal',
				    							);

                    $this->init_form_fields();

                    $this->availability       = $this->get_option( 'availability' );
					$this->enabled            = $this->get_option( 'enabled' );
					$this->title              = $this->get_option( 'title' );
					$this->description 		  = $this->get_option( 'description' );
					$this->cost               = $this->get_option( 'cost' );
                
          			add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
          
				}
 
				
        
        
        		function init_form_fields() {
					
          			$this->instance_form_fields = array(
						'enabled' => array(
							'title'       => __( 'Povolit PPL Parcelshop', 'woo-ppl-parcelshop' ),
							'type' 	      => 'checkbox',
							'label'       => __( 'Povolit tento způsob', 'woo-ppl-parcelshop' ),
							'default'     => 'no',
							),
						'title' => array(
							'title'       => __( 'Název metody', 'woo-ppl-parcelshop' ),
							'type'        => 'text',
							'description' => __( 'Název metody', 'woo-ppl-parcelshop' ),
							'default'     => __( 'PPL Parcelshop', 'woo-ppl-parcelshop' ),
							),
						'description' => array(
							'title'       => __( 'Vyzvednutí na pobočce PPL Parcelshop', 'woo-ppl-parcelshop' ),
							'type'        => 'text',
							'description' => __( 'Text, který zákazník uvidí', 'woo-ppl-parcelshop' ),
							'default'     => 'Vyzvednutí na pobočce PPL Parcelshop',
						),
            			'cost' => array(
							'title'       => __( 'Cena za dopravu', 'woo-ppl-parcelshop' ),
							'type'        => 'price',
							'description' => __( 'Cena za dopravu', 'woo-ppl-parcelshop' ),
							'default'     => '90',
						),
						'dobirka' => array(
							'title'       => __( 'Příplatek za dobírku', 'woo-ppl-parcelshop' ),
							'type'        => 'price',
							'description' => __( 'Příplatek za dobírku', 'woo-ppl-parcelshop' ),
							'default'     => '20',
						)

					);

				}
        
        
 
				/**
				 * calculate_shipping function.
				 *          
				 * @since 1.0.0
				 */
				public function calculate_shipping( $package = array() ) {
					
          			//Define rates array
          			$rates = array();
          			//Create rates important values
					$rates['id']       = $this->id. '>' .$this->instance_id;
          			$rates['label']    = $this->title;
          			$rates['calc_tax'] = 'per_order';
          			$rates['cost']     = $this->cost;
          
          			$this->add_rate( $rates );
            
         
				}
        
        
      
    			/**
    			 * Is available shipping
    			 *
    			 */                    
    			public function is_available( $package = array() ){
    
      				if ( 'no' == $this->enabled ) {
						return false;
					}             

		    		return apply_filters( 'woocommerce_shipping_' . $this->id . '_is_available', true, $package );
      
    			} 
        
			}//End class 
      
		}
  
}
add_action('plugins_loaded', 'woocommerce_pplparcelshop_shipping_init');
 
function add_woo_pplparcelshop_shipping_method( $methods ) {
	$methods['pplparcelshop'] = 'WC_Pplparcelshop_Shipping_Method';
	return $methods;
}
add_filter( 'woocommerce_shipping_methods', 'add_woo_pplparcelshop_shipping_method' );



?>