<?php 

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

$dirl =  dirname(dirname(dirname(dirname(__FILE__))));
require_once($dirl.'/wp-load.php');   
require_once($dirl.'/wp-includes/option.php');

if(!function_exists('ppl_prevod_znaku')){
  function ppl_prevod_znaku( $pobocka ){
      $prevodni_tabulka = Array(
          'ä'=>'a',
          'Ä'=>'A',
          'á'=>'a',
          'Á'=>'A',
          'à'=>'a',
          'À'=>'A',
          'ã'=>'a',
          'Ã'=>'A',
          'â'=>'a',
          'Â'=>'A',
          'č'=>'c',
          'Č'=>'C',
          'ć'=>'c',
          'Ć'=>'C',
          'ď'=>'d',
          'Ď'=>'D',
          'ě'=>'e',
          'Ě'=>'E',
          'é'=>'e',
          'É'=>'E',
          'ë'=>'e',
          'Ë'=>'E',
          'è'=>'e',
          'È'=>'E',
          'ê'=>'e',
          'Ê'=>'E',
          'í'=>'i',
          'Í'=>'I',
          'ï'=>'i',
          'Ï'=>'I',
          'ì'=>'i',
          'Ì'=>'I',
          'î'=>'i',
          'Î'=>'I',
          'ľ'=>'l',
          'Ľ'=>'L',
          'ĺ'=>'l',
          'Ĺ'=>'L',
          'ń'=>'n',
          'Ń'=>'N',
          'ň'=>'n',
          'Ň'=>'N',
          'ñ'=>'n',
          'Ñ'=>'N',
          'ó'=>'o',
          'Ó'=>'O',
          'ö'=>'o',
          'Ö'=>'O',
          'ô'=>'o',
          'Ô'=>'O',
          'ò'=>'o',
          'Ò'=>'O',
          'õ'=>'o',
          'Õ'=>'O',
          'ő'=>'o',
          'Ő'=>'O',
          'ř'=>'r',
          'Ř'=>'R',
          'ŕ'=>'r',
          'Ŕ'=>'R',
          'š'=>'s',
          'Š'=>'S',
          'ś'=>'s',
          'Ś'=>'S',
          'ť'=>'t',
          'Ť'=>'T',
          'ú'=>'u',
          'Ú'=>'U',
          'ů'=>'u',
          'Ů'=>'U',
          'ü'=>'u',
          'Ü'=>'U',
          'ù'=>'u',
          'Ù'=>'U',
          'ũ'=>'u',
          'Ũ'=>'U',
          'û'=>'u',
          'Û'=>'U',
          'ý'=>'y',
          'Ý'=>'Y',
          'ž'=>'z',
          'Ž'=>'Z',
          'ź'=>'z',
          'Ź'=>'Z'
      );

      $pobocka = strtr($pobocka, $prevodni_tabulka);

      return $pobocka;

  }
}


global $wpdb;
    try {
        
        $ktm = new SoapClient('https://www.pplbalik.cz/ASM/Ktm.asmx?WSDL');
        $pobocky = $ktm->GetKTMList();
        
        if(!empty($pobocky->GetKTMListResult->KTMDetail)){
        
        $wpdb->query("TRUNCATE TABLE `".$wpdb->prefix."ppl_parcelshop`");    
            
            $i = 1;

            $tabulka_option = array();
            
            foreach($pobocky->GetKTMListResult->KTMDetail as $item){
            

              var_dump( $item->city );
              echo '<br />';

              $gpsN          = trim((string)$item->gpsN);
              $gpsE          = trim((string)$item->gpsE);
              $position      = trim((string)$item->position);
              $openTime      = trim((string)$item->openTime);
              $customerName1 = trim((string)$item->customerName1);
              $KTMID         = trim((string)$item->KTMID);
              $city          = trim((string)$item->city);
              $street1       = trim((string)$item->street1);
              $zipCode       = trim((string)$item->zipCode);
              $customerId    = trim((string)$item->customerId);
              $customerDepId = trim((string)$item->customerDepId);

              $pobocka = ppl_prevod_znaku( $city );

              $tabulka_option[$pobocka] = $city;

              $var_2 = strtolower( $pobocka );
              $var_3 = strtolower( $city );

              $tabulka_option[$var_2] = $city;
              $tabulka_option[$var_3] = $city;
             
              $result = $wpdb->insert($wpdb->prefix.'ppl_parcelshop', 
	                                 array( 
		                                      'ID'            => $i,
                                          'gpsN'          => $gpsN,
                                          'gpsE'          => $gpsE,
                                          'position'      => $position,
                                          'openTime'      => $openTime,
                                          'customerName1' => $customerName1,
                                          'KTMID'         => $KTMID,
                                          'city'          => $city,
                                          'street1'       => $street1,
                                          'zipCode'       => $zipCode,
                                          'customerId'    => $customerId,
                                          'customerDepId' => $customerDepId
	                                      ),
                                        array(
                                          '%d',     
                                          '%s',
                                          '%s',
                                          '%s',
                                          '%s',
                                          '%s',
                                          '%s',
                                          '%s',
                                          '%s',
                                          '%s',
                                          '%s',
                                          '%s'
                                        ) 
                            );
        
             $i++;
        
            }
        }
        
    }catch (SoapFault $f) {
     
        var_dump($f);

		}	

    update_option( 'ppl_parcelshop_tabulka', $tabulka_option );