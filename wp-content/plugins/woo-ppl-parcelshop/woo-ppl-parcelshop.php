<?php
/**
 * @package   Woo PPL Parcelshop
 * @author    Toret.cz
 * @license   GPL-2.0+
 * @link      https://toret.cz
 * @copyright 2015 Toret.cz
 *
 * Plugin Name:       Woo PPL Parcelshop
 * Plugin URI:        
 * Description:       Doručení Na pobočku PPL Parcelshopu
 * Version:           1.1.8
 * Author:            toret.cz
 * Author URI:        toret.cz
 * Text Domain:       pplparcelshop
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


define( 'PPLPARCELSHOPDIR', plugin_dir_path( __FILE__ ) );
define( 'PPLPARCELSHOPURL', plugin_dir_url( __FILE__ ) );

require_once( plugin_dir_path( __FILE__ ) . 'includes/plugin-update-checker-master/plugin-update-checker.php' );
$MyUpdateChecker = PucFactory::buildUpdateChecker(
    'http://update-server.toret.cz/wp-update-server-master/?action=get_metadata&slug=woo-ppl-parcelshop', 
    __FILE__,
    'woo-ppl-parcelshop' 
);

require_once( plugin_dir_path( __FILE__ ) . 'includes/compatibility/toret_compatibility.php' );

/*----------------------------------------------------------------------------*
 * Public-Facing Functionality
 *----------------------------------------------------------------------------*/

require_once( plugin_dir_path( __FILE__ ) . 'admin/includes/setting.php' );
require_once( plugin_dir_path( __FILE__ ) . 'includes/class-wc-gateway-pplparcelshop.php' );
require_once( plugin_dir_path( __FILE__ ) . 'includes/pplparcelshop.php' );
require_once( plugin_dir_path( __FILE__ ) . 'includes/pplparcelshop-kuryr.php' );
require_once( plugin_dir_path( __FILE__ ) . 'includes/class-wc-gateway-dobirka.php' );


require_once( plugin_dir_path( __FILE__ ) . 'public/class-woo-ppl-parcelshop.php' );

register_activation_hook( __FILE__, array( 'PPL_Parcelshop', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'PPL_Parcelshop', 'deactivate' ) );

add_action( 'plugins_loaded', array( 'PPL_Parcelshop', 'get_instance' ) );

/*----------------------------------------------------------------------------*
 * Dashboard and Administrative Functionality
 *----------------------------------------------------------------------------*/

if ( is_admin() && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {

	require_once( plugin_dir_path( __FILE__ ) . 'admin/class-woo-ppl-parcelshop-admin.php' );
	add_action( 'plugins_loaded', array( 'PPL_Parcelshop_Admin', 'get_instance' ) );

}

/**
 * Calculate fee
 */ 
add_action( 'woocommerce_cart_calculate_fees' , 'calculate_parcelshop_fee' );

function calculate_parcelshop_fee(){
  
    if ( ($current_gateway = ppl_parcelshop_get_current_gateway()) && ($settings = ppl_parcelshop_get_current_gateway_settings()) ) {
    
        if( $current_gateway->id == 'dobirka' ){
       
            $doprava_name = explode( '>',WC()->session->chosen_shipping_methods[0] );
            if( isset( $doprava_name[1] ) ){
                $instance_id = $doprava_name[1];
                if( $doprava_name[0] == 'pplparcelshop' ){
                    $option = get_option('woocommerce_pplparcelshop_'.$instance_id.'_settings');
                }
                elseif( $doprava_name[0] == 'ppl-parcelshop-kuryr'){ 
                    $option = get_option('woocommerce_ppl-parcelshop-kuryr_'.$instance_id.'_settings');
                }
       
                if( isset( $option['dobirka'] ) ){
                                     
                    WC()->cart->add_fee( __( 'Příplatek za Dobírku', 'pplparcelshop' ) , $option['dobirka'], true );
             
                }
            }
        }
    }
}

  
/**
 * Get current gateway
 *
 * since 1.0.0
 */
    
function ppl_parcelshop_get_current_gateway(){
  
  	$available_gateways = WC()->payment_gateways->get_available_payment_gateways();
		
    $current_gateway = null;
		
    $default_gateway = get_option( 'woocommerce_default_gateway' );
		if ( ! empty( $available_gateways ) ) {

		   // Chosen Method
			if ( isset( WC()->session->chosen_payment_method ) && isset( $available_gateways[ WC()->session->chosen_payment_method ] ) ) {
				
        $current_gateway = $available_gateways[ WC()->session->chosen_payment_method ];
			
      } elseif ( isset( $available_gateways[ $default_gateway ] ) ) {
				$current_gateway = $available_gateways[ $default_gateway ];
			} else {
				$current_gateway = current( $available_gateways );
			}
		}
		if ( ! is_null( $current_gateway ) )
			return $current_gateway;
		else 
			return false;
}


/**
 * Current payment gateway setting
 *
 * since 1.0.0
 */   
 
function ppl_parcelshop_get_current_gateway_settings( ) {
		if ( $current_gateway = ppl_parcelshop_get_current_gateway() ) {
			$settings = $current_gateway->settings;
			return $settings;
		}
		return false;
}


/**
 * Recalcute cart
 *
 * since 1.0.0
 */
if (!function_exists('woo_print_autoload_js')) { 
add_action( 'woocommerce_review_order_after_submit' , 'woo_print_autoload_js' );

function woo_print_autoload_js(){
		?><script type="text/javascript">
        jQuery(document).ready(function($){
	         $(document.body).on('change', 'input[name="payment_method"]', function() {
		       jQuery('body').trigger('update_checkout');
	       });
        });
 		</script><?php 
}

}


/**
 * Remove all shipping methods, when free is available
 *
 * since 1.0.0
 */  
if(!function_exists('ppl_doprava_hide_shipping_when_free_is_available')){
  add_filter( 'woocommerce_package_rates', 'ppl_doprava_hide_shipping_when_free_is_available', 10, 2 );
  function ppl_doprava_hide_shipping_when_free_is_available( $rates, $package ) {
  	if ( version_compare( WOOCOMMERCE_VERSION, '2.6.0', '>=' ) ) {

      $free = false;
      foreach ( $rates as $rate_id => $rate ) {       
        if ( 'free_shipping' === $rate->method_id ) {         
          $free = true;          
          $free_rate_id = $rate_id;
          break;        
        }     
      }

      if ( $free === true ) {
        foreach($rates as $key => $item){
          $rates[$key]->cost  = 0;
          $rates[$key]->tax   = 0;
          $rates[$key]->taxes = false;
        }
      
        unset( $rates[$free_rate_id] );      
      }
    
    }else{  
      
      if ( isset( $rates['free_shipping'] ) ) {
        foreach($rates as $key => $item){
          $rates[$key]->cost  = 0;
          $rates[$key]->tax   = 0;
          $rates[$key]->taxes = false;
        }      
        unset( $rates['free_shipping'] );      
      }         
      
    }   
     
    return $rates;
  }
}      


/**
 * Get all whisper data
 *
 */
add_action('wp_ajax_parcelshop_branches', 'parcelshop_get_branches');
add_action('wp_ajax_nopriv_parcelshop_branches', 'parcelshop_get_branches');   
function parcelshop_get_branches(){
   global $wpdb;

   $pobocka = sanitize_text_field( $_POST['dotaz'] );


    $data = $wpdb->get_results( 
             "SELECT gpsE, gpsN, position, openTime, customerName1, KTMID, city, street1, zipCode FROM ".$wpdb->prefix."ppl_parcelshop WHERE zipCode LIKE '" . $pobocka . "%'"
    );

  
    if( empty( $data ) ){

        $data = $wpdb->get_results( 
	         "SELECT gpsE, gpsN, position, openTime, customerName1, KTMID, city, street1, zipCode FROM ".$wpdb->prefix."ppl_parcelshop WHERE city LIKE '" . $pobocka . "%'"
        );

        if( empty( $data ) ){
      
            $pobocky = get_option( 'ppl_parcelshop_tabulka' );
            if( !empty( $pobocky[$pobocka] ) ){

                $data = $wpdb->get_results( 
                    "SELECT gpsE, gpsN, position, openTime, customerName1, KTMID, city, street1, zipCode FROM ".$wpdb->prefix."ppl_parcelshop WHERE city LIKE '" . $pobocky[$pobocka] . "%'"
                );

            }
 
        }

    }

    if( !empty( $data ) ){
     $html  = '<table class="table-parcelshop">';
     $html .= '<tr><td>'.__('Pobočka','woo-ppl-parcelshop').'</td><td>'.__('PSČ','woo-ppl-parcelshop').'</td><td>'.__('Adresa','woo-ppl-parcelshop').'</td><td></td></tr>';
     foreach($data as $item){
        $html .= '<tr><td>'.$item->customerName1.'</td><td>'.$item->zipCode.'</td><td>'.$item->street1.', '.$item->city.'</td><td><a href="#" class="pplvybrat-button" data-pobocka="'.$item->KTMID.'">'.__('Vybrat','woo-ppl-parcelshop').'</a></td></tr>';
     
     }
     $html .= '</table>';
   
    }else{
        $html = _e('<p>Nenalezeny žádné pobočky, zadejte prosím jiný název.</p>','woo-ppl-parcelshop');
    }

   echo $html;
   exit();
}

if(!function_exists('ppl_prevod_znaku')){
  function ppl_prevod_znaku( $pobocka ){
      $prevodni_tabulka = Array(
          'ä'=>'a',
          'Ä'=>'A',
          'á'=>'a',
          'Á'=>'A',
          'à'=>'a',
          'À'=>'A',
          'ã'=>'a',
          'Ã'=>'A',
          'â'=>'a',
          'Â'=>'A',
          'č'=>'c',
          'Č'=>'C',
          'ć'=>'c',
          'Ć'=>'C',
          'ď'=>'d',
          'Ď'=>'D',
          'ě'=>'e',
          'Ě'=>'E',
          'é'=>'e',
          'É'=>'E',
          'ë'=>'e',
          'Ë'=>'E',
          'è'=>'e',
          'È'=>'E',
          'ê'=>'e',
          'Ê'=>'E',
          'í'=>'i',
          'Í'=>'I',
          'ï'=>'i',
          'Ï'=>'I',
          'ì'=>'i',
          'Ì'=>'I',
          'î'=>'i',
          'Î'=>'I',
          'ľ'=>'l',
          'Ľ'=>'L',
          'ĺ'=>'l',
          'Ĺ'=>'L',
          'ń'=>'n',
          'Ń'=>'N',
          'ň'=>'n',
          'Ň'=>'N',
          'ñ'=>'n',
          'Ñ'=>'N',
          'ó'=>'o',
          'Ó'=>'O',
          'ö'=>'o',
          'Ö'=>'O',
          'ô'=>'o',
          'Ô'=>'O',
          'ò'=>'o',
          'Ò'=>'O',
          'õ'=>'o',
          'Õ'=>'O',
          'ő'=>'o',
          'Ő'=>'O',
          'ř'=>'r',
          'Ř'=>'R',
          'ŕ'=>'r',
          'Ŕ'=>'R',
          'š'=>'s',
          'Š'=>'S',
          'ś'=>'s',
          'Ś'=>'S',
          'ť'=>'t',
          'Ť'=>'T',
          'ú'=>'u',
          'Ú'=>'U',
          'ů'=>'u',
          'Ů'=>'U',
          'ü'=>'u',
          'Ü'=>'U',
          'ù'=>'u',
          'Ù'=>'U',
          'ũ'=>'u',
          'Ũ'=>'U',
          'û'=>'u',
          'Û'=>'U',
          'ý'=>'y',
          'Ý'=>'Y',
          'ž'=>'z',
          'Ž'=>'Z',
          'ź'=>'z',
          'Ź'=>'Z'
      );

      $pobocka = strtr($pobocka, $prevodni_tabulka);

      return $pobocka;

  }
}

/**
 * Get one branches
 *
 */
add_action('wp_ajax_parcelshop_simple', 'parcelshop_get_simple');
add_action('wp_ajax_nopriv_parcelshop_simple', 'parcelshop_get_simple');   
function parcelshop_get_simple(){
   global $wpdb;

   $idecko = sanitize_text_field($_POST['idecko']);

   $data = $wpdb->get_results( 
	         "SELECT gpsE, gpsN, position, openTime, customerName1, KTMID, city, street1, zipCode FROM ".$wpdb->prefix."ppl_parcelshop WHERE KTMID = '" . $idecko ."'"
   );

   if(!empty($data)){
     $html  = '<table class="table-parcelshop">';
     $html .= '<tr><td>'.__('Pobočka','woo-ppl-parcelshop').'</td><td>'.__('PSČ','woo-ppl-parcelshop').'</td><td>'.__('Adresa','woo-ppl-parcelshop').'</td><td></td></tr>';
     foreach($data as $item){
        $html .= '
                  <tr>
                      <td>'.$item->customerName1.'</td>
                      <td>'.$item->zipCode.'</td>
                      <td>'.$item->street1.' '.$item->city.'</td>
                      <td><a href="#" class="pplsmazat-button" data-pobocka="'.$item->KTMID.'">'.__('Smazat','woo-ppl-parcelshop').'</a></td>
                      <input type="hidden" name="parcelshop-nazev" value="'.$item->customerName1.'" />
                      <input type="hidden" name="parcelshop-psc" value="'.$item->zipCode.'" />
                      <input type="hidden" name="parcelshop-adresa" value="'.$item->street1.' '.$item->city.'" />
                      <input type="hidden" name="parcelshop-id" value="'.$item->KTMID.'" />
                      <input type="hidden" name="parcelshop-gpsE" value="'.$item->gpsE.'" />
                      <input type="hidden" name="parcelshop-gpsN" value="'.$item->gpsN.'" />
                      <input type="hidden" name="parcelshop-position" value="'.$item->position.'" />
                      <input type="hidden" name="parcelshop-openTime" value="'.$item->openTime.'" />
                  </tr>';
 
     }
     $html .= '</table>';
   
   }else{
     $html = __('<p>Chyba při načítání pobočky, prosím zkuste znovu.</p>', 'woo-ppl-parcelshop');
   }

   echo $html;
   exit();
}

/**
 *
 * Uložit id místa
 *
 */  
add_action( 'woocommerce_checkout_update_order_meta', 'ppl_parcelshop_store_pickup_field_update_order_meta' );
function ppl_parcelshop_store_pickup_field_update_order_meta( $order_id ) {
  if ( $_POST[ 'parcelshop-nazev' ] ){
    update_post_meta( $order_id, 'parcelshop_pobocka_nazev', esc_attr( $_POST[ 'parcelshop-nazev' ] ) );
  }
  if ( $_POST[ 'parcelshop-psc' ] ){
    update_post_meta( $order_id, 'parcelshop_pobocka_psc', esc_attr( $_POST[ 'parcelshop-psc' ] ) );
  }
  if ( $_POST[ 'parcelshop-adresa' ] ){
    update_post_meta( $order_id, 'parcelshop_pobocka_adresa', esc_attr( $_POST[ 'parcelshop-adresa' ] ) );
  }
  if ( $_POST[ 'parcelshop-id' ] ){
    update_post_meta( $order_id, 'parcelshop_pobocka_id', esc_attr( $_POST[ 'parcelshop-id' ] ) );
  } 
  if ( $_POST[ 'parcelshop-gpsE' ] ){
    update_post_meta( $order_id, 'parcelshop_pobocka_gpsE', esc_attr( $_POST[ 'parcelshop-gpsE' ] ) );
  } 
    if ( $_POST[ 'parcelshop-gpsN' ] ){
    update_post_meta( $order_id, 'parcelshop_pobocka_gpsN', esc_attr( $_POST[ 'parcelshop-gpsN' ] ) );
  } 
    if ( $_POST[ 'parcelshop-position' ] ){
    update_post_meta( $order_id, 'parcelshop_pobocka_position', esc_attr( $_POST[ 'parcelshop-position' ] ) );
  } 
    if ( $_POST[ 'parcelshop-openTime' ] ){
    update_post_meta( $order_id, 'parcelshop_pobocka_openTime', esc_attr( $_POST[ 'parcelshop-openTime' ] ) );
  } 
    
}

