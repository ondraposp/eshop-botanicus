<?php
/**
 * @package   Woo Balikonos
 * @author    toret.cz
 * @license   GPL-2.0+
 * @link      http://toret.cz
 * @copyright 2015 Toret.cz
 */
 
?>

<div class="wrap">

	<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>

	<p>Přehled WordPress pluginů</p>

</div>
