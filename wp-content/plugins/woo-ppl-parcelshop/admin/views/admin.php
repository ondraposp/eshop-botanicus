<?php
/**
 * @package   Woo PPL Parcelshop
 * @author    toret.cz
 * @license   GPL-2.0+
 * @link      http://toret.cz
 * @copyright 2015 Toret.cz
 */
 
if(isset($_POST['control'])){ 
  if(!empty($_POST['licence'])){
    if(trim($_POST['licence'])!=''){  
      woo_ppl_parcelshop_control_licence($_POST['licence']); 
    }
  }
  wp_redirect(get_admin_url().'admin.php?page=woo-ppl-parcelshop');
   
} 

if(isset($_GET['load'])){
    global $wpdb;
    try {
        
        $ktm = new SoapClient('https://www.pplbalik.cz/ASM/Ktm.asmx?WSDL');
        $pobocky = $ktm->GetKTMList();
        
        if(!empty($pobocky->GetKTMListResult->KTMDetail)){
        
        $wpdb->query("TRUNCATE TABLE `".$wpdb->prefix."ppl_parcelshop`");    
            
            $i = 1;
            
            $tabulka_option = array();

            foreach($pobocky->GetKTMListResult->KTMDetail as $item){
            
              $gpsN          = trim((string)$item->gpsN);
              $gpsE          = trim((string)$item->gpsE);
              $position      = trim((string)$item->position);
              $openTime      = trim((string)$item->openTime);
              $customerName1 = trim((string)$item->customerName1);
              $KTMID         = trim((string)$item->KTMID);
              $city          = trim((string)$item->city);
              $street1       = trim((string)$item->street1);
              $zipCode       = trim((string)$item->zipCode);
              $customerId    = trim((string)$item->customerId);
              $customerDepId = trim((string)$item->customerDepId);
            
              $pobocka = ppl_prevod_znaku( $city );

              $tabulka_option[$pobocka] = $city;

              $var_2 = strtolower( $pobocka );
              $var_3 = strtolower( $city );

              $tabulka_option[$var_2] = $city;
              $tabulka_option[$var_3] = $city;

              $result = $wpdb->insert($wpdb->prefix.'ppl_parcelshop', 
	                                 array( 
		                                  'ID'            => $i,
                                          'gpsN'          => $gpsN,
                                          'gpsE'          => $gpsE,
                                          'position'      => $position,
                                          'openTime'      => $openTime,
                                          'customerName1' => $customerName1,
                                          'KTMID'         => $KTMID,
                                          'city'          => $city,
                                          'street1'       => $street1,
                                          'zipCode'       => $zipCode,
                                          'customerId'    => $customerId,
                                          'customerDepId' => $customerDepId
	                                      ),
                                        array(
                                          '%d',     
                                          '%s',
                                          '%s',
                                          '%s',
                                          '%s',
                                          '%s',
                                          '%s',
                                          '%s',
                                          '%s',
                                          '%s',
                                          '%s',
                                          '%s'
                                        ) 
                            );
        
             $i++;
        
            }
        }
        
    }catch (SoapFault $f) {
     
        var_dump($f);

		}	


    update_option( 'ppl_parcelshop_tabulka', $tabulka_option );
  
  wp_redirect(get_admin_url().'admin.php?page=woo-ppl-parcelshop');
  
}

  
$option = get_option('woo-ppl-parcelshop-option');

$licence_key  = get_option('woo-ppl-parcelshop-licence-key');
$licence_info = get_option('woo-ppl-parcelshop-info');
global $lic;    
    
    
?>

<div class="wrap">

	<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>

	<div class="t-col-6">
    <div class="toret-box box-info">
      <div class="box-header">
        <h3 class="box-title"><?php _e('Zadejte licenční klíč',$this->plugin_slug); ?></h3>
      </div>
      <div class="box-body">
        <?php if(!empty($licence_info)){ ?>
          <p><strong><?php echo $licence_info; ?></strong></p>
        <?php
            delete_option('woo-ppl-parcelshop-info');  
          }
        ?>
        <?php if(!empty($lic)){ ?>
          <p><strong>Vaše licence je aktivní.</strong></p>
        <?php
          }
        ?>

        <form method="post" style="margin-bottom:10px;">
          <input type="text" name="licence" id="licence" style="width:400px;" value="<?php if(!empty($licence_key)){ echo $licence_key; } ?>" />
          <input type="hidden" name="control" value="ok" />
          <input type="submit" class="btn btn-info btn-sm " value="<?php _e('Ověřit licenci',$this->plugin_slug); ?>" />
        </form>
      </div>
    </div>
  </div>   
  
<div style="clear:both;"></div>  
  
  
  <div class="t-col-6">
    <div class="toret-box box-info">
      <div class="box-header">
        <h3 class="box-title"><?php _e('Nastavení pluginu',$this->plugin_slug); ?></h3>
      </div>
      <div class="box-body">
         <p><?php _e('Načtení poboček aktualizuje data o pobočkách PPL Parcelshop v databázi.',$this->plugin_slug); ?></p>
         <a href="<?php echo get_admin_url().'admin.php?page=woo-ppl-parcelshop&load=ok' ?>" class="btn btn-info btn-sm "><?php _e('Načíst pobočky PPL Parcelshopu',$this->plugin_slug); ?></a>
         <br />
         <p><?php _e('Url pro automatickou aktualizaci poboček.', 'woo-ppl-parcelshop'); ?></p>
         <p><strong><?php echo PPLPARCELSHOPURL.'parcelshop-update.php'; ?></strong></p>
      </div>
    </div>
  </div>
  
<div style="clear:both;"></div>  
 
<div class="t-col-12">
    <div class="toret-box box-info">
      <div class="box-header">
        <h3 class="box-title"><?php _e('Pobočky Parcelshopu',$this->plugin_slug); ?></h3>
      </div>
      <div class="box-body">
      <table class="table-bordered">  
      <?php 
      global $wpdb;
      $data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."ppl_parcelshop");
        if(!empty($data)){
        $i = 1;
          foreach($data as $item){ ?>
            <tr>
              <td><?php echo $i; ?></td>
              <td><?php echo $item->customerName1; ?></td>
              <td><?php echo $item->street1; ?></td>
              <td><?php echo $item->city; ?></td>
              <td><?php echo $item->zipCode; ?></td>
              <td><?php echo $item->position; ?></td>
            </tr>
        <?php    
          $i++;
          }
        }
      ?>
      </table>
      </div>
    </div>
</div>    
  
<div style="clear:both;"></div>    
  

</div>
