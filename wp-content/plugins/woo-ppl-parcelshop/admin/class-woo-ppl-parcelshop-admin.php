<?php
/**
 * @package   Woo PPL Parcelshop
 * @author    toret.cz
 * @license   GPL-2.0+
 * @link      http://toret.cz
 * @copyright 2015 Toret.cz
 */
 
class PPL_Parcelshop_Admin {

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Slug of the plugin screen.
	 *
	 * @since    1.0.0
	 *
	 * @var      string
	 */
	protected $plugin_screen_hook_suffix = null;


	/**
	 * Initialize the plugin by loading admin scripts & styles and adding a
	 * settings page and menu.
	 *
	 * @since     1.0.0
	 */
	private function __construct() {

		$plugin = PPL_Parcelshop::get_instance();
		$this->plugin_slug = $plugin->get_plugin_slug();

		// Load admin style sheet and JavaScript.
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_styles' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_scripts' ) );

		// Add the options page and menu item.
		add_action( 'admin_menu', array( $this, 'add_plugin_admin_menu' ) );

		// Add an action link pointing to the options page.
		$plugin_basename = plugin_basename( plugin_dir_path( __DIR__ ) . $this->plugin_slug . '.php' );
		add_filter( 'plugin_action_links_' . $plugin_basename, array( $this, 'add_action_links' ) );

    
		$licence_status = get_option('woo-ppl-parcelshop-licence');
    if ( empty( $licence_status ) ) {
	     return false;
    }
    
    
    /**
     *  Output fix
     */              
    add_action('admin_init', array( $this, 'output_buffer' ) );
    add_action('woocommerce_admin_order_data_after_shipping_address', array( $this, 'order_ppl_parcelshop_address' ) );

	}

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Register and enqueue admin-specific style sheet.
	 *
	 * @since     1.0.0
	 *
	 * @return    null    Return early if no settings page is registered.
	 */
	public function enqueue_admin_styles() {

			wp_enqueue_style( $this->plugin_slug .'-admin-styles', plugins_url( 'assets/css/admin.css', __FILE__ ), array(), PPL_Parcelshop::VERSION );

	}

	/**
	 * Register and enqueue admin-specific JavaScript.
	 *
	 * @since     1.0.0
	 *
	 * @return    null    Return early if no settings page is registered.
	 */
	public function enqueue_admin_scripts() {

			wp_enqueue_script( $this->plugin_slug . '-admin-script', plugins_url( 'assets/js/admin.js', __FILE__ ), array( 'jquery' ), PPL_Parcelshop::VERSION );

	}

	/**
	 * Register the administration menu for this plugin into the WordPress Dashboard menu.
	 *
	 * @since    1.0.0
	 */
	public function add_plugin_admin_menu() {

		if (!defined('TORETMENU')) {
     
     	add_menu_page(
			__( 'Toret plugins', $this->plugin_slug ),
			__( 'Toret plugins', $this->plugin_slug ),
			'manage_woocommerce',
			'toret-plugins',
			array( $this, 'display_toret_plugins_admin_page' ),
        	PPLPARCELSHOPURL.'assets/t-icon.png'
		);
     
     	define( 'TORETMENU', true );
  	}
  
  
  	
	add_submenu_page(
			'toret-plugins',
     		__( 'PPL Parcelshop', $this->plugin_slug ),
			__( 'PPL Parcelshop', $this->plugin_slug ),
			'manage_woocommerce',
			$this->plugin_slug,
			array( $this, 'display_plugin_admin_page' )
		);

	}

  	/**
	 * Render the settings page for all plugins
	 *
	 * @since    1.0.0
	 */
	public function display_toret_plugins_admin_page() {
		include_once( 'views/toret.php' );
	}

	/**
	 * Render the settings page for this plugin.
	 *
	 * @since    1.0.0
	 */
	public function display_plugin_admin_page() {
		include_once( 'views/admin.php' );
	}

	/**
	 * Add settings action link to the plugins page.
	 *
	 * @since    1.0.0
	 */
	public function add_action_links( $links ) {

		return array_merge(
			array(
				'settings' => '<a href="' . admin_url( 'options-general.php?page=' . $this->plugin_slug ) . '">' . __( 'Settings', $this->plugin_slug ) . '</a>'
			),
			$links
		);

	}
  
  
  /**
	 * Headers allready sent fix
	 * 
	 * @since    1.0.0        
	 */
	public function output_buffer() {
		ob_start();
	}
  

    /**
     * Show full address in order detail
     *
     * @since 1.0.0
     */              
    public function order_ppl_parcelshop_address($order){
    
    	$order_id = Toret_Order_Compatibility::get_order_id( $order );

        $nazev    = get_post_meta( $order_id, 'parcelshop_pobocka_nazev', true );

        if( empty( $nazev ) ){ return; }
        
        $psc      = get_post_meta( $order_id, 'parcelshop_pobocka_psc', true );
        $adresa   = get_post_meta( $order_id, 'parcelshop_pobocka_adresa', true );
        $id       = get_post_meta( $order_id, 'parcelshop_pobocka_id', true );
        $gpsE     = get_post_meta( $order_id, 'parcelshop_pobocka_gpsE', true );
        $gpsN     = get_post_meta( $order_id, 'parcelshop_pobocka_gpsN', true );
        $position = get_post_meta( $order_id, 'parcelshop_pobocka_position', true );
        $openTime = get_post_meta( $order_id, 'parcelshop_pobocka_openTime', true );

    
      
        if(!empty($id)){
          echo '<h4>'.__('PPL Parcelshop',$this->plugin_slug).'</h4>';
          $html = '<div class="address"><strong>'.__('Adresa pobočky: ', $this->plugin_slug).'</strong>'.$nazev.', '.$adresa.', '.$psc.'</div>';
            
            echo apply_filters( 'ppl_parcelshop_admin_order_note', $html, $order );      
            
        }
    
    }

}
