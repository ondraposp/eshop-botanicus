<?php
/**
 * @package   Woo PPL Parcelshop
 * @author    toret.cz
 * @license   GPL-2.0+
 * @link      http://toret.cz
 * @copyright 2015 Toret.cz
 */

class PPL_Parcelshop {

	/**
	 * Plugin version, used for cache-busting of style and script file references.
	 *
	 * @since   1.0.0
	 *
	 * @var     string
	 */
	const VERSION = '1.1.8';

	/**
	 *
	 * @since    1.0.0
	 *
	 * @var      string
	 */
	protected $plugin_slug = 'woo-ppl-parcelshop';

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Initialize the plugin by setting localization and loading public scripts
	 * and styles.
	 *
	 * @since     1.0.0
	 */
	private function __construct() {

		// Load plugin text domain
		add_action( 'init', array( $this, 'load_plugin_textdomain' ) );

		// Activate plugin when new blog is added
		add_action( 'wpmu_new_blog', array( $this, 'activate_new_site' ) );

		// Load public-facing style sheet and JavaScript.
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

    
		$licence_status = get_option('woo-ppl-parcelshop-licence');
    	if ( empty( $licence_status ) ) {
	    	return false;
    	}
    
   
        //Add ajax url
        add_action('wp_head', array( $this, 'ajaxurl' ) );
   
        add_action('woocommerce_review_order_after_shipping', array( $this, 'select_option' ) );
        //add_action('wp_head', array($this, 'check_select') );
   
        add_action( 'woocommerce_email_after_order_table', array( $this, 'add_ppl_parcelshop_email' ), 15, 2 );
        add_action( 'woocommerce_order_details_after_order_table', array( $this, 'add_ppl_parcelshop_my_order_email' ), 10, 1 );

        add_action('woocommerce_checkout_process', array( $this, 'my_custom_checkout_field_process' ) );
        
    
	}

	/**
	 * Return the plugin slug.
	 *
	 * @since    1.0.0
	 *
	 * @return    Plugin slug variable.
	 */
	public function get_plugin_slug() {
		return $this->plugin_slug;
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Fired when the plugin is activated.
	 *
	 * @since    1.0.0
	 *
	 * @param    boolean    $network_wide    True if WPMU superadmin uses
	 *                                       "Network Activate" action, false if
	 *                                       WPMU is disabled or plugin is
	 *                                       activated on an individual blog.
	 */
	public static function activate( $network_wide ) {

		if ( function_exists( 'is_multisite' ) && is_multisite() ) {

			if ( $network_wide  ) {

				// Get all blog ids
				$blog_ids = self::get_blog_ids();

				foreach ( $blog_ids as $blog_id ) {

					switch_to_blog( $blog_id );
					self::single_activate();
				}

				restore_current_blog();

			} else {
				self::single_activate();
			}

		} else {
			self::single_activate();
		}

	}

	/**
	 * Fired when the plugin is deactivated.
	 *
	 * @since    1.0.0
	 *
	 * @param    boolean    $network_wide    True if WPMU superadmin uses
	 *                                       "Network Deactivate" action, false if
	 *                                       WPMU is disabled or plugin is
	 *                                       deactivated on an individual blog.
	 */
	public static function deactivate( $network_wide ) {

		if ( function_exists( 'is_multisite' ) && is_multisite() ) {

			if ( $network_wide ) {

				// Get all blog ids
				$blog_ids = self::get_blog_ids();

				foreach ( $blog_ids as $blog_id ) {

					switch_to_blog( $blog_id );
					self::single_deactivate();

				}

				restore_current_blog();

			} else {
				self::single_deactivate();
			}

		} else {
			self::single_deactivate();
		}

	}

	/**
	 * Fired when a new site is activated with a WPMU environment.
	 *
	 * @since    1.0.0
	 *
	 * @param    int    $blog_id    ID of the new blog.
	 */
	public function activate_new_site( $blog_id ) {

		if ( 1 !== did_action( 'wpmu_new_blog' ) ) {
			return;
		}

		switch_to_blog( $blog_id );
		self::single_activate();
		restore_current_blog();

	}

	/**
	 * Get all blog ids of blogs in the current network that are:
	 * - not archived
	 * - not spam
	 * - not deleted
	 *
	 * @since    1.0.0
	 *
	 * @return   array|false    The blog ids, false if no matches.
	 */
	private static function get_blog_ids() {

		global $wpdb;

		// get an array of blog ids
		$sql = "SELECT blog_id FROM $wpdb->blogs
			WHERE archived = '0' AND spam = '0'
			AND deleted = '0'";

		return $wpdb->get_col( $sql );

	}

	/**
	 * Fired for each blog when the plugin is activated.
	 *
	 * @since    1.0.0
	 */
	private static function single_activate() {

    global $wpdb;

		$wpdb->hide_errors();

		$collate = '';

		if ( $wpdb->has_cap( 'collation' ) ) {
			if ( ! empty($wpdb->charset ) ) {
				$collate .= "DEFAULT CHARACTER SET $wpdb->charset";
			}
			if ( ! empty($wpdb->collate ) ) {
				$collate .= " COLLATE $wpdb->collate";
			}
		}

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        $table = "
	         CREATE TABLE {$wpdb->prefix}ppl_parcelshop (
              `ID` bigint(21) NOT NULL,
              `gpsN` varchar(100) COLLATE utf8_czech_ci NOT NULL,
              `gpsE` varchar(100) COLLATE utf8_czech_ci NOT NULL,
              `position` longtext COLLATE utf8_czech_ci NOT NULL,
              `openTime` longtext COLLATE utf8_czech_ci NOT NULL,
              `customerName1` varchar(100) COLLATE utf8_czech_ci NOT NULL,
              `KTMID` varchar(100) COLLATE utf8_czech_ci NOT NULL,
              `city` varchar(100) COLLATE utf8_czech_ci NOT NULL,
              `street1` varchar(100) COLLATE utf8_czech_ci NOT NULL,
              `zipCode` varchar(100) COLLATE utf8_czech_ci NOT NULL,
              `customerId` varchar(100) COLLATE utf8_czech_ci NOT NULL,
              `customerDepId` varchar(100) COLLATE utf8_czech_ci NOT NULL,
              PRIMARY KEY (`ID`)
	         ) $collate;
	      ";
		    dbDelta( $table );
  

	}

	/**
	 * Fired for each blog when the plugin is deactivated.
	 *
	 * @since    1.0.0
	 */
	private static function single_deactivate() {

	}

	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		$domain = $this->plugin_slug;
		$locale = apply_filters( 'plugin_locale', get_locale(), $domain );

		load_textdomain( $domain, trailingslashit( PPLPARCELSHOPDIR ) . 'languages/' . $domain . '-' . $locale . '.mo' );

	}

	/**
	 * Register and enqueue public-facing style sheet.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		wp_enqueue_style( $this->plugin_slug . '-plugin-styles', plugins_url( 'assets/css/public.css', __FILE__ ), array(), self::VERSION );
	}

	/**
	 * Register and enqueues public-facing JavaScript files.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		wp_enqueue_script( $this->plugin_slug . '-plugin-script', plugins_url( 'assets/js/public.js', __FILE__ ), array( 'jquery' ), self::VERSION );
        $translate = array(
            'misto_doruceni'        => __('Zadejte místo doručení!','woo-ppl-parcelshop')
        );
        wp_localize_script($this->plugin_slug . '-plugin-script', 'script_strings', $translate);
	}
  
  	/**
  	 * Select option
  	 *
  	 */        
  	public function select_option(){
  	
  		$doprava_name = explode( '>',WC()->session->chosen_shipping_methods[0] );
        if( isset( $doprava_name[1] ) ){
            if( $doprava_name[0] == 'pplparcelshop' ){
  
    			echo '<tr>';
      				echo '<th>'.__('Vyberte pobočku','woo-ppl-parcelshop').'</th>';
        			echo '<td>';
          				echo '<input type="text" name="parcelshop_select" id="parcelshop_select" placeholder="'.__('Zadejte město','woo-ppl-parcelshop').'">';
          				echo '&nbsp;<a href="#" class="button alt" id="vyhledat_parcelshop">'.__('Vyhledat','woo-ppl-pacelshop').'</a>';
          				echo '<div id="pplvyhledane_pobocky" style="display:none;"></div>';
          				echo '<div id="pplvybrana_pobocka" style="display:none;"></div>';
        				echo '<input type="hidden" name="selected_parcelshop" id="pplselected_parcelshop" value="0" />';
      				echo '</td>';
    			echo '</tr>';

  			}

  		}
  
  	}
  
  	/**
  	 * Check select function
  	 *
  	 */        
  	public function check_select(){
  		if ( WC()->cart->needs_shipping() ){
  		?>
  		<script type="text/javascript">
        	jQuery(document).ready(function($){

	        	jQuery('body').on('click', '#place_order', function() {
		       		var shipping = $( '.shipping_method:checked' ).val();

		       		if( )

           			var pobocka = jQuery('#selected_parcelshop').val();
          			if(pobocka == '0'){
            			alert('Prosím, vyberte pobočku PPL Parcelshop.');
            			return false;
           			}
           
	       		});


        	});
  		</script>
  		<?php
  		}
  	}
  
  
   /**
  	 *
  	 * Add address to customer email
  	 *   
  	 */        
  	public function add_ppl_parcelshop_email($order,$is_admin ){
  		
  		$order_id = Toret_Order_Compatibility::get_order_id( $order );
    
    	$nazev    = get_post_meta( $order_id, 'parcelshop_pobocka_nazev', true );

    	if( empty( $nazev ) ){ return; }

    	$psc      = get_post_meta( $order_id, 'parcelshop_pobocka_psc', true );
    	$adresa   = get_post_meta( $order_id, 'parcelshop_pobocka_adresa', true );
    	$id       = get_post_meta( $order_id, 'parcelshop_pobocka_id', true );
    	$gpsE     = get_post_meta( $order_id, 'parcelshop_pobocka_gpsE', true );
    	$gpsN     = get_post_meta( $order_id, 'parcelshop_pobocka_gpsN', true );
    	$position = get_post_meta( $order_id, 'parcelshop_pobocka_position', true );
    	$openTime = get_post_meta( $order_id, 'parcelshop_pobocka_openTime', true );

    
      
        $html = '<p style="color:#000000">'.__('Zásilka bude odeslána na adresu zvolené pobočky:','woo-ppl-parcelshop').' '.$adresa.'</p>';
    
        echo apply_filters( 'ppl_parcelshop_email_note', $html, $order );      
    
  }
    
    

  	/**
  	 *
  	 * Add address to customer email
  	 *   
  	 */        
  	public function add_ppl_parcelshop_my_order_email($order){

  		$order_id = Toret_Order_Compatibility::get_order_id( $order );
    
    	$nazev    = get_post_meta( $order_id, 'parcelshop_pobocka_nazev', true );

    	if( empty( $nazev ) ){ return; }

    	$psc      = get_post_meta( $order_id, 'parcelshop_pobocka_psc', true );
    	$adresa   = get_post_meta( $order_id, 'parcelshop_pobocka_adresa', true );
    	$id       = get_post_meta( $order_id, 'parcelshop_pobocka_id', true );
    	$gpsE     = get_post_meta( $order_id, 'parcelshop_pobocka_gpsE', true );
    	$gpsN     = get_post_meta( $order_id, 'parcelshop_pobocka_gpsN', true );
    	$position = get_post_meta( $order_id, 'parcelshop_pobocka_position', true );
    	$openTime = get_post_meta( $order_id, 'parcelshop_pobocka_openTime', true );

    
      
        $html = '<p style="color:#000000">'.__('Zásilka bude odeslána na adresu zvolené pobočky:','woo-ppl-parcelshop').' '.$adresa.'</p>';
    
        echo apply_filters( 'ppl_parcelshop_my_order_note', $html, $order );      
    
  	}



  	/**
  	 * Define Ajax url for frontend
  	 * @since 1.1.0   
  	 */ 
  	public function ajaxurl() {
    	?>
      	<script type="text/javascript">
        	var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
      	</script>
    	<?php
  	} 

  	public function my_custom_checkout_field_process() {
    
    	// Check if set, if its not set add an error.
    	if ( $_POST['shipping_method'][0] ){
    		$doprava_name = explode( '>', $_POST['shipping_method'][0] );
    		if( isset( $doprava_name[1] ) ){
                if( $doprava_name[0] == 'pplparcelshop' ){
                	if ( ! $_POST['selected_parcelshop'] ){
        				wc_add_notice( __( 'Prosím, vyberte pobočku Parcelshopu','woo-ppl-parcelshop' ), 'error' );
    				}
    	    	}	
    		}
    	}
	
	}
                







}//End class
