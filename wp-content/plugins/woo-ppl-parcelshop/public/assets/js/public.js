(function ($) {
	"use strict";

	$(function () {

		jQuery('#order_review').on('click', '#vyhledat_parcelshop', function (e){
            e.preventDefault();
            var dotaz = jQuery('#parcelshop_select').val();
            if(!dotaz){
                alert(script_strings.misto_doruceni);
                return;
            }
            var data = {
                action: 'parcelshop_branches',
                dotaz : dotaz
            };
            jQuery.post(ajaxurl, data, function(response){
                jQuery('#pplvybrana_pobocka').empty();
                jQuery('#pplvybrana_pobocka').css('display','none');
                jQuery('#pplvyhledane_pobocky').empty();
                jQuery('#pplvyhledane_pobocky').css('display','block');
                jQuery('#pplvyhledane_pobocky').append(response);
            });  
        });
    
    
        jQuery('#order_review').on('click', '.pplsmazat-button', function (e){
            e.preventDefault();
            jQuery('#order_review .table-parcelshop').remove();
            sessionStorage.parcelshopPplVybranaPobockaHtml = null;
            sessionStorage.parcelshopPplVybranaPobocka = null;
        });
        
        jQuery('#order_review').on('click','.pplvybrat-button',function(e){
        
            e.preventDefault();
            var idecko = jQuery(this).data('pobocka');
         
            var data = {
                action : 'parcelshop_simple',
                idecko : idecko
            };
            jQuery.post(ajaxurl, data, function(response){
                jQuery('#pplvyhledane_pobocky').css('display','none');
                jQuery('#pplvybrana_pobocka').css('display','block');
                jQuery('#pplvybrana_pobocka').append(response);
                jQuery('#pplselected_parcelshop').val(idecko);
                sessionStorage.parcelshopPplVybranaPobockaHtml = jQuery('#pplvybrana_pobocka').html();
                sessionStorage.parcelshopPplVybranaPobocka = jQuery('#pplselected_parcelshop').val();
            });  
    
        });

        jQuery( document.body ).on( 'updated_checkout', function() {
            console.log(sessionStorage.parcelshopPplVybranaPobocka);
            if(typeof sessionStorage.parcelshopPplVybranaPobocka !== "undefined") {
                jQuery('#pplvybrana_pobocka').empty();
                jQuery('#pplvyhledane_pobocky').css('display','none');
                jQuery('#parcelshop_select').css('display','none');
                jQuery('#vyhledat_parcelshop').css('display','none');
                jQuery('#pplvybrana_pobocka').css('display','block');
                jQuery('#pplvybrana_pobocka').append(sessionStorage.parcelshopPplVybranaPobockaHtml);
                jQuery('#pplselected_parcelshop').val(sessionStorage.parcelshopPplVybranaPobocka);
                jQuery('.vyhledat_parcelshop').remove();
            }
        }); 
	});

}(jQuery));