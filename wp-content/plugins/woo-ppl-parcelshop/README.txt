﻿=== Woo PPL Parcelshop ===
Requires at least: 3.8
Tested up to: 4.0
Stable tag: 1.0.0
License: 
Version: 1.1.8
License URI: 

== Popis ==
* Plugin umožňuje vybrat pobočku PPL Parcelshopu pro doručení zásilky

== Instalace ==
* Plugin je možné nainstalovat pomocí FTP, nebo nahráním Zip souboru v administraci

= Minimální požadavky =
* WordPress 3.8 nebo vyšší
* PHP version 5.2.4 nebo vyšší
* MySQL version 5.0 nebo vyšší

== Changelog ==

https://toret.cz/produkt/woo-ppl-parcelshop/
