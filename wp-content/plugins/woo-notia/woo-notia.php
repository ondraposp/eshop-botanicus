<?php

/**
* Plugin Name: Woo Notia
* Plugin URI: none
* Description: Plugin for connection Woo commerce with Notia API.
* Version: 1.0.0
* Author: Ondrej Pospisil
* License: GPL2
*/

define('ONDRAP_NOTIA_DIR', plugin_dir_path(__FILE__));
define('ONDRAP_NOTIA_URL', plugin_dir_url(__FILE__));
define('ONDRAP_NOTIA_PLUGIN_BASENAME', plugin_basename(__FILE__));
define('ONDRAP_NOTIA_VERSION', '1.0.0');

// Localize plugin
add_action('init', 'ondrap_notia_localize_plugin');

function ondrap_notia_localize_plugin() {
    load_plugin_textdomain('woo-notia', false, basename( dirname( __FILE__ ) ) . '/languages');
}

// Is WooCommerce active?
add_action('plugins_loaded', 'ondrap_notia_plugin_init');

function ondrap_notia_plugin_init() {

    // If WooCommerce is NOT active
    if (current_user_can('activate_plugins') && !class_exists('woocommerce')) {

	add_action('admin_init', 'kbnt_mailstep_deactivate'); //todo
	add_action('admin_notices', 'kbnt_mailstep_admin_notice'); //todo

	// IF WooCommerce IS ACTIVE
    } else {

		require_once( ONDRAP_NOTIA_DIR . 'includes/add-order-status.php' ); //todo
		
		if (is_admin()) {
			add_action('admin_enqueue_scripts', 'kbnt_mailstep_admin_scripts'); //todo
			add_filter('woocommerce_get_settings_pages', 'kbnt_mailstep_woocommerce_get_settings_pages'); //todo
			add_filter('plugin_action_links_' . ONDRAP_NOTIA_PLUGIN_BASENAME, 'kbnt_mailstep_plugin_action_links'); //todo
		}

		if (is_kbnt_mailstep_active()) { //todo
			require_once( ONDRAP_NOTIA_DIR . 'includes/class-kbnt-mailstep-api.php' ); //todo
			require_once( ONDRAP_NOTIA_DIR . 'includes/class-kbnt-mailstep-wc-ajax.php' ); //todo
			require_once( ONDRAP_NOTIA_DIR . 'includes/class-kbnt-mailstep-orders.php' ); //todo
		}
    }
}

// On plugin deactivation
register_deactivation_hook( __FILE__, 'ondrap_notia_plugin_deactivation' );
function ondrap_notia_plugin_deactivation() {

	// update all notia statuses to pending
	$notia_orders = ondrap_notia_orders_with_custom_status();
	foreach ( $notia_orders as $no ) {
		$order = new WC_Order( $no );
		$order->update_status( 'processing', 'Notia plugin byl deaktivován: ' ); 
	}

}

// Orders with a custom status
function ondrap_notia_orders_with_custom_status() {
	// todo
	$notia_custom = array( 'wc-newnotia', 'wc-pendinggoods', 'wc-pickingup', 'wc-completion', 'wc-topick', 'wc-handedcourier', 'wc-notiapaid');

	$orders = wc_get_orders( array(
		'limit'    => -1,
		'status'   => $notia_custom
	) );

	$notia_orders = array();

	foreach ( $orders as $order ) {
		$notia_orders[] = $order->get_ID();
	}

	return $notia_orders;
}